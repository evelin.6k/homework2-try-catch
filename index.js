const books = [
    { 
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70 
    }, 
    {
     author: "Скотт Бэккер",
     name: "Воин-пророк",
    }, 
    { 
      name: "Тысячекратная мысль",
      price: 70
    }, 
    { 
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
    }, 
    {
     author: "Дарья Донцова",
     name: "Детектив на диете",
     price: 40
    },
    {
     author: "Дарья Донцова",
     name: "Дед Снегур и Морозочка",
    }
  ];

const root = document.getElementById('root');
const neededProperties = ['author','name','price'];
const ul = document.createElement('ul');
root.append(ul);

books.forEach(book => {
try {
   neededProperties.forEach(property => {
       if(!book.hasOwnProperty(property)) {
            throw new Error(`Book "${book.name}" dont have ${property}`)
       }
    })
    for (property in book){
        const li = document.createElement('li');
        li.innerText = `${property} : ${book[property]}`
        ul.append(li);
    };
    }
catch (err) {
    console.log(err);
    }
})